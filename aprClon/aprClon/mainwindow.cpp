#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QtWidgets>
#include <QtGui>

//=====================================================================================
#define slova_size 100

QString ImpFile;    /*файл из апрола*/
QString CsvFile;    /*файл шо куда менять*/
QString SaveFile;   /*файл куда сохранять*/
QStringList slova[slova_size];
QStringList implist,savelist;
int max_p;          /*кол-во строк в словах*/
int max_i, I = 1;
int protoNo = 0;    /*номер столбика в csv - прототипа*/

QString Poehali(int i1)
{
    QString str;
    str.setNum(i1);//I
    return "Поехали " + str + "!";
}

//=====================================================================================

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->label_3->setText("");
}

MainWindow::~MainWindow()
{
    delete ui;
}

int ProtoNo()//вычисляем номер прототипа
{
    if (max_p>0)
    {
        for(int pi=0;pi<qMin(max_p,5);pi++)//ограничение в 5 записи - только по первым трём строчкам клоншаблона определяем прототип. Остальные строчки могут содержать разные цифры
        {
            QString strPart = slova[pi][0];
            //QStringList strList = strPart.split(QRegExp("[1234567890]"));
            int posP = strPart.indexOf(QRegExp("[0123456789]"));
            QString strP = strPart.mid(0, posP);// strList[0];
            int pos = 0;
            QString strFound = "";
            QString stroka;

            for(int ri=0; ri<implist.count(); ri++)
            {
                stroka = implist[ri];
                pos = stroka.lastIndexOf(strP);
                if (pos>0) {strFound = implist[ri].right(implist[ri].length() - pos); break;}
            }

            if (pos>0)
            {
                int pos1 = strFound.indexOf(QRegExp("[1234567890]"), strP.length());
                int pos2 = strFound.indexOf(QRegExp("[^1234567890]"), pos1);
                QString str_no = strFound.mid(pos1, pos2-pos1);
                //QString str_no = strFound.mid(0, pos2);
                //int nomer = str_no.toInt();
                if ( ( str_no.length() > 1) && (str_no.length() - strP.length() < 4))//4 - это максимальная разрядность номеров станций (0 - 9999)
                {
                    int im_no = -1;
                    for(int im=0;im<max_i;im++)
                    {
                        int pos3 = slova[pi][im].indexOf(str_no);
                        if (pos3>-1)
                        {
                            im_no = im;
                            break;
                        }
                    }
                    if ((im_no>=0)&&(im_no<max_i))
                        return im_no;
                }
            }
        }

    }
    return 0;
}

QString CheckNumbers(int protoNumber)//проверяем есть ли разные номера ключевых слов в имп
{
    if (max_p>0)
    {
        for(int pi=0;pi<max_p;pi++)
        {
            QString strPart = slova[pi][0];
            //QStringList strList = strPart.split(QRegExp("[1234567890]"));
            int posP = strPart.indexOf(QRegExp("[0123456789]"));
            QString strP = strPart.mid(0, posP - 1);// strList[0];
            int pos = 0;
            QString strFound = "";
            QString stroka;

            for(int ri=0; ri<implist.count(); ri++)
            {
                stroka = implist[ri];
                pos = stroka.lastIndexOf(strP);
                if (pos>0)
                {
                    strFound = implist[ri].right(implist[ri].length() - pos);



                    int pos1 = strFound.indexOf(QRegExp("[1234567890]"), strP.length());
                    int pos2 = strFound.indexOf(QRegExp("[^1234567890]"), pos1);
                    QString str_no = strFound.mid(pos1, pos2-pos1);
                    //QString str_no = strFound.mid(0, pos2);
                    //int nomer = str_no.toInt();
                    if ( ( str_no.length() > 1) && (str_no.length() - strP.length() < 4))//4 - это максимальная разрядность номеров станций (0 - 9999)
                    {
                        int im_no = -1;
                        for(int im=0;im<max_i;im++)
                        {
                            int pos3 = slova[pi][im].indexOf(str_no);
                            if (pos3>-1)
                            {
                                im_no = im;
                                break;
                            }
                        }
                        if ((im_no>=0)&&(im_no<max_i)&&(im_no!=protoNumber))
                            return "Imp содержит не родной номер ключевого слова!!! " + strFound + "\n" + implist[ri].left(30);
                    }
                 }
            }
        }

    }
    return 0;
}

void MainWindow::on_pushButton_clicked()//open *.imp
{
    QString settingsFile = "settings.apr";
        //читаем из файла настроек последний открытый путь
        QFile fiS(settingsFile);
        QString pathI = "";
        QString pathC = "";
        if (fiS.open(QIODevice::ReadOnly))
        {
            QTextStream streamIS;
            streamIS.setCodec("UTF-8");
            streamIS.setDevice(&fiS);
            if (!streamIS.atEnd())
                      pathI = streamIS.readLine();//первая запись - для imp
            if (!streamIS.atEnd())
                      pathC = streamIS.readLine();
            fiS.close();
        }



    ImpFile = "";
    ui->checkBox->setChecked(false);
    ImpFile = QFileDialog::getOpenFileName(this, tr("Выберите imp-файл импорта из АПРОЛа"), pathI, "*.imp");
    if (ImpFile.length()>3)
    {

        ui->pushButton_4->setEnabled(false);
        ui->textBrowser->clear();
        savelist.clear();
        implist.clear();

        QFile fi(ImpFile);
        if (!fi.open(QIODevice::ReadOnly)) return;
        QTextStream stream;
        QString str1;
        stream.setCodec("UTF-8");
        stream.setDevice(&fi);
        while (!stream.atEnd())
        {
            str1 = stream.readLine();
            ui->textBrowser->append(str1);
            implist.append(str1);
        }
        fi.close();
        ui->label->setText("imp-файл загружен");
        ui->checkBox->setChecked(true);
        I = 1;
        ui->pushButton_3->setText(Poehali(I));
    }

    protoNo = ProtoNo();
    if (protoNo<max_i)
        ui->label_2->setText("прототип: " + slova[0][protoNo]);

    QString strErr = CheckNumbers(protoNo);
     ui->label_3->setText(strErr.left(strErr.indexOf("\n")));
     ui->textBrowser->append(strErr);



    //записываем в файл настроек последний открытый путь
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QFile foS(settingsFile);
    if (!foS.open(QIODevice::WriteOnly)) return;
    QTextStream streamOS(&foS);
    QString path = ImpFile.left(ImpFile.lastIndexOf("/"));
    streamOS << path;
    streamOS << "\n";
    streamOS << pathC;
    streamOS << "\n";
    foS.close();
}

void MainWindow::on_pushButton_2_clicked()//open *.csv
{
    QString settingsFile = "settings.apr";
        //читаем из файла настроек последний открытый путь
        QFile fiS(settingsFile);
        QString pathI = "";
        QString pathC = "";
        if (fiS.open(QIODevice::ReadOnly))
        {
            QTextStream streamIS;
            streamIS.setCodec("UTF-8");
            streamIS.setDevice(&fiS);
            if (!streamIS.atEnd())
                      pathI = streamIS.readLine();
            if (!streamIS.atEnd())
                      pathC = streamIS.readLine();//вторая запись - для csv
            fiS.close();
        }



    int p= 0;
    CsvFile = "";
    ui->checkBox_2->setChecked(false);
    CsvFile = QFileDialog::getOpenFileName(this, tr("Выберите csv-файл шо куда менять"), pathC, "*.csv");
    if (CsvFile.length()>3)
    {
        ui->pushButton_4->setEnabled(false);
        ui->textBrowser_2->clear();
        savelist.clear();
        QFile fi(CsvFile);
        if (!fi.open(QIODevice::ReadOnly)) return;
        QTextStream stream;
        QString str1;
        stream.setCodec("UTF-8");
        stream.setDevice(&fi);
        while (!stream.atEnd())
        {
            str1 = stream.readLine();
            if (str1.length()>3)
            {
                ui->textBrowser_2->append(str1);

                if (p>=slova_size) { ui->label->setText("слишком много строк в csv"); return; }
                slova[p++] = str1.split(QRegExp("[,;]"));
            }
        }
        fi.close();
        I = 1;
        Poehali(I);
        max_i = slova[0].count();
        max_p = p;
        ui->checkBox_2->setChecked(true);
        ui->label->setText("csv-файл загружен");
        I = 1;
        ui->pushButton_3->setText(Poehali(I));
    }

    protoNo = ProtoNo();
    if (protoNo<max_i)
        ui->label_2->setText("прототип: " + slova[0][protoNo]);

    QString strErr = CheckNumbers(protoNo);
     ui->label_3->setText(strErr.left(strErr.indexOf("\n")));
     ui->textBrowser->append(strErr);

    //записываем в файл настроек последний открытый путь
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QFile foS(settingsFile);
    if (!foS.open(QIODevice::WriteOnly)) return;
    QTextStream streamOS(&foS);
    streamOS << pathI;
    streamOS << "\n";
    QString path = CsvFile.left(CsvFile.lastIndexOf("/"));
    streamOS << path;
    streamOS << "\n";
    foS.close();
}

QString zamena(QString str1, unsigned long int Ilocal)
{
    int p,pos1;
    QString str;
    if ( (str1.length()>1) && (str1[1]!='#'))
    {
        for(p=0; p<max_p; p++)
        {
            str = str1; str1 = "";
            QString strProto = slova[p][protoNo];
            while ( (pos1 = str.indexOf(strProto)) > 0 )
            {
                str1 += str.mid(0,pos1);
                str1 += slova[p][Ilocal];
                str.remove(0, pos1 + strProto.length());
            }
            str1 += str;
        }//for p
    }
    return str1;
}


bool SaveImp(QString savefile1)
{
    //SaveFile = "";
    //SaveFile = QFileDialog::getSaveFileName(this, tr("Введите имя файла"), SaveFile, "*.imp");
    SaveFile = savefile1;
    if (SaveFile.length()>3)
    {
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
        QFile fout(SaveFile);
        if (fout.open(QIODevice::WriteOnly) ==false) return false;
        QTextStream stream(&fout);
        for(int i=0; i < savelist.count(); i++)
        {
            stream << savelist.value(i);
            stream << "\n";
        }

        fout.close();

        return true;
    }
    return false;
}

bool SaveImp()
{
    return SaveImp(SaveFile);
}

void MainWindow::on_pushButton_4_clicked()//сохранить
{
    SaveFile = QFileDialog::getSaveFileName(this, tr("Введите имя файла"), SaveFile, "*.imp");
    if (SaveImp(SaveFile))
        ui->label->setText("Сохранено");
    else
        ui->label->setText("НЕ СОХРАНЕНО!!!");
}

bool ZamenaOneTime(int numberI, bool doRazlozhiPoPapkam)
{
    long int ri;

    QString str1;

    savelist.clear();
    for(ri=0; ri<implist.count(); ri++)
    {
        str1 = zamena(implist[ri],numberI);

        savelist.append(str1);
    }

    if (doRazlozhiPoPapkam)
    {
        int posSlash = ImpFile.lastIndexOf("/");
        QString dir = ImpFile.left(posSlash);
        QString subdir = slova[0][numberI];
        QDir().mkdir(dir + "/" + subdir);
        QString fullname = zamena(ImpFile, numberI);
        if (fullname==ImpFile)
        {
            fullname = fullname.left(fullname.length()-4) + "_" + slova[0][numberI] + ".imp";
        }
        QString filename = fullname.right(fullname.length() - posSlash - 1);

        SaveFile = dir + "/" + subdir + "/" + filename;
    }
    else
    {
        SaveFile = zamena(ImpFile, numberI);
        if (SaveFile==ImpFile)
        {
            SaveFile = SaveFile.left(SaveFile.length()-4) + "_" + slova[0][numberI] + ".imp";
        }
    }



    return true;
}

void MainWindow::on_pushButton_3_clicked()//поехали
{
    if ((I>=1) && (I<max_i)&&(ui->checkBox->isChecked())&&(ui->checkBox_2->isChecked()))
    {
        ui->textBrowser_2->clear();
        QString str;

        ZamenaOneTime(I, ui->checkBox_3->isChecked());

        for(long int i=0;i<savelist.count();i++)
            ui->textBrowser_2->append(savelist[i]);

        str.setNum(I);
        ui->label->setText("Результат №" + str + " (" + slova[1][I] + "...)");

        if (I<max_i-1) I++; else I = 1;

        ui->pushButton_3->setText(Poehali(I));
        ui->pushButton_4->setEnabled(true);

    }
}

void MainWindow::on_pushButton_6_clicked()
{
    if (I>1) I--;
    ui->pushButton_3->setText(Poehali(I));
}

void MainWindow::on_pushButton_5_clicked()
{
    if (I<max_i) I++;
    ui->pushButton_3->setText(Poehali(I));
}



void MainWindow::on_pushButton_7_clicked()//автомат
{
    bool doRazlozhiPoPapkam = ui->checkBox_3->isChecked();

    int cnt = 0;
    bool are_errors = false;
    for(int i1=0;i1<max_i;i1++)
    {
        if (i1!=protoNo)
        {
            I = i1;
            ZamenaOneTime(i1, doRazlozhiPoPapkam);
            if (SaveImp())
            {
                cnt++;
            }
            else
            {
                are_errors = true;
            }

        }
    }
    QString str;
    QString str1 = "";
    str.setNum(cnt);
    if (are_errors) str1 = "!!! С ОШИБКАМИ !!!";
    ui->label->setText("Сохранено " + str + " штук " + str1);

}


