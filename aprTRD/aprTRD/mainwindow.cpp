#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QtWidgets>

//==========================================
#define MAX_STR_CNT 10000
#define YSTEP 15//шаг блоков по У
#define LINEKOL 7//кол-во линий к блоку
#define KOL_TRDS_PER_PAGE 3//кол-во тренд-блоков на листе
#define MAX_PAGES_PER_PD 25//макс кол-во страниц в ПД
static bool isCSVchoosed;
static int noVarBegin = 0;
static int noVarEnd = 1;
static int noPage2Save = 0;
static QString strPDName = "AI_trd";
static int iTrendsAlarms = 0;//0 = trends, 1 = alarms


//static int V_p;
struct varia{
    QString AprolName;  /*Name*/
    QString PlcName;
    QString VarType;
    QString ShortDesc;  /*INPUT-OUTPUT*/
    QString Desc;       /*Description RUS*/
    QString Velichina;  /*Unit*/
    QString MSRno;      /*MSR No*/
    QString Min;        /*MIN*/
    QString Max;        /*MAX*/
    QString TOL;        /*TRD Tolerance*/
    QString DescENG;    /*Description ENG*/
};
static class vari{
public:
    varia Lines;
    void operator=(QString csv_line);
    int count = 0;
} V[MAX_STR_CNT];
//-
enum cfcStepEnum{
    eHEAD,
    eVARS,
    ePAGE,
    eBLOCKS,
    eCONNECTORS,
    eVarREFS,
    ePodval
};

#define SHABLON_SIZE 50
#define HEADER_SIZE_MAX 50

static QStringList SaveLines;  /*результат для сохранения в файл*/
static QStringList ErrorLines;  /*списки ошибок*/
static qint64 currpos = 0;

enum variaEnum{
    eAprolName,
    ePlcName,
    eVarType,
    eDesc001,
    eVelichina,
    eMSRno,
    eMin,
    eMax,
    eNone,
    eDesc007,
    eInputOutput,
    ePoints,
    eNumber,
    eNumber2,
    eEmpty,
    eTolerance,
};

struct lineZamena{
    QString strBefore;
    QString strAfter;
    variaEnum type;
    QList<int> pntsX;
    QList<int> pntsY;
    int iNumber;
};

//------------------------------------

void vari::operator =(QString csv_line)
{
    QStringList list1 = csv_line.split(QRegExp("[;]"));
    this->Lines.AprolName = list1.value(0);
    this->Lines.PlcName   = list1.value(1);
    this->Lines.VarType   = list1.value(2);
    this->Lines.ShortDesc = list1.value(3);
    this->Lines.Desc      = list1.value(4);
    this->Lines.Velichina = list1.value(5);
    this->Lines.MSRno     = list1.value(6);
    QString str7 = list1.value(7);
    if (str7.length()==0)
    {
        if (this->Lines.VarType=="BOOL") {
            str7 = "-1";
        }
        else {
            str7 = "0";
        }
    }
    this->Lines.Min       = str7;
    QString str8 = list1.value(8);
    if (str8.length()==0)
    {
        if (this->Lines.VarType=="BOOL") {
            str8 = "2";
        }
        else {
            str8 = "100";
        }
    }
    this->Lines.Max       = str8;
    if (list1.count()>9)
        this->Lines.TOL   = list1.value(9);
    else
        this->Lines.TOL   = "0.5";
    this->Lines.DescENG      = list1.value(10);
}

QString Translit(QString strin)
{
    QString strout = "";
    QString str = "";

    for(int i = 0; i < strin.length(); i++)
    {
        str = strin[i];

        if (str=="а") strout += "a";
        //else if (str=="г") strout += "r";
        else if (str=="е") strout += "e";
        else if (str=="и") strout += "u";
        else if (str=="о") strout += "o";
        else if (str=="п") strout += "n";
        else if (str=="р") strout += "p";
        else if (str=="с") strout += "c";
        else if (str=="т") strout += "m";
        else if (str=="у") strout += "y";
        else if (str=="х") strout += "x";
        else if (str=="А") strout += "A";
        else if (str=="В") strout += "B";
        else if (str=="Е") strout += "E";
        else if (str=="Ё") strout += "E";
        else if (str=="З") strout += "3";
        else if (str=="К") strout += "K";
        else if (str=="М") strout += "M";
        else if (str=="Н") strout += "H";
        else if (str=="О") strout += "O";
        else if (str=="Р") strout += "P";
        else if (str=="С") strout += "C";
        else if (str=="Т") strout += "T";
        else if (str=="У") strout += "У";
        else if (str=="Х") strout += "X";
        else if (str=="ё") strout += "e";
        else strout += str;

    }
    if (strout.length()<1) strout=" ";
    return strout;
}

QString TranslitLight(QString strin)
{
    QString strout = "";
    QString str = "";

    for(int i = 0; i < strin.length(); i++)
    {
        str = strin[i];

        if (str=="у") strout += "y";
        else if (str=="х") strout += "x";
        else if (str=="Е") strout += "E";
        else if (str=="Ё") strout += "E";
        else if (str=="З") strout += "3";
        else if (str=="Р") strout += "P";
        else if (str=="С") strout += "C";
        else if (str=="Т") strout += "T";
        else if (str=="У") strout += "У";
        else if (str=="Х") strout += "X";
        else if (str=="ё") strout += "e";
        else strout += str;

    }
    if (strout.length()<1) strout=" ";
    return strout;
}

//==========================================

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

int CountVariablesPerTRD_PD()
{
    int kol = MAX_PAGES_PER_PD * KOL_TRDS_PER_PAGE;
    return kol;
}

QString Zamena(QString str1)
{

    int pos = str1.indexOf(",");
    while (pos>0)
    {
        str1 = str1.replace(pos,1,".");
        pos = str1.indexOf(",");
    }
    pos = str1.indexOf("\"");
    while (pos>0)
    {
        str1 = str1.replace(pos,1,"");
        pos = str1.indexOf("\"");
    }

    return str1;
}

int CountOfSymbol(QString str1, QChar symbol)
{
    int cn = 0;
    for(int i=0;i<str1.length();i++)
    {
        if (str1[i]==symbol) cn++;
    }
    return cn;
}




void MainWindow::on_pushButton_clicked()//open csv
{
    QString settingsFile = "settings.apr";
        //читаем из файла настроек последний открытый путь
        QFile fiS(settingsFile);
        QString pathI = "";
        QString pathC = "";
        if (fiS.open(QIODevice::ReadOnly))
        {
            QTextStream streamIS;
            streamIS.setCodec("UTF-8");
            streamIS.setDevice(&fiS);
            if (!streamIS.atEnd())
                      pathI = streamIS.readLine();
            if (!streamIS.atEnd())
                      pathC = streamIS.readLine();//вторая запись - для csv
            fiS.close();
        }




    //читаем CSV-файл
    QString CsvFile = "";
    CsvFile = QFileDialog::getOpenFileName(this, tr("Выберите CSV-файл"), pathC, "*.csv");
    if (CsvFile.length()>3) isCSVchoosed = true; else { isCSVchoosed = false; return; }

    ui->checkBox->setChecked(isCSVchoosed);
    ui->textBrowser->clear();

    QFile fi(CsvFile);
    if (!fi.open(QIODevice::ReadOnly)) return;
    QTextStream stream;
    QString str1;
    stream.setCodec("UTF-8");
    stream.setDevice(&fi);
    //V_p = 0;
    V->count = 0;
    if (ui->checkBox_4->isChecked()) str1 = stream.readLine();/*пропустить первую строку CSV-файла*/
    bool flagMessage = true;
    while (!stream.atEnd())
    {
        str1 = stream.readLine();

        if (flagMessage)
        {
            if (CountOfSymbol(str1,',')>3)
            {
                ui->textBrowser_2->append("При сохранении CSV укажите разделитель - точку с запятой!");
                ui->textBrowser_2->append("Запятая используется для дробных чисел");
                flagMessage = false;
            }
        }
        str1 = Zamena(str1);


        V[V->count] = str1;
        if (V[V->count].Lines.AprolName.length()>1)
        {
            //V_p++;
            V->count++;
            ui->textBrowser->append(str1);
        }
        //else
        //    str1="";
    }
    fi.close();
    ui->label_5->setText("всего "+QString::number(V->count));
    noPage2Save = 0;
    if (V->count>CountVariablesPerTRD_PD()-1)
    {
        noVarBegin = 0;
        noVarEnd = CountVariablesPerTRD_PD()-1;
    }
    else
    {
        noVarBegin = 0;
        noVarEnd = V->count;
    }
    ui->spinBox->setValue(noVarBegin);
    ui->spinBox_2->setValue(noVarEnd);




    //записываем в файл настроек последний открытый путь
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QFile foS(settingsFile);
    if (!foS.open(QIODevice::WriteOnly)) return;
    QTextStream streamOS(&foS);
    streamOS << pathI;
    streamOS << "\n";
    QString path = CsvFile.left(CsvFile.lastIndexOf("/"));
    streamOS << path;
    streamOS << "\n";
    foS.close();
}




int Read2ZamenaByKeyWord(int cnt, QString str1, QString strKeyword, QString strKeyword2, variaEnum enum1, lineZamena *zamena)
{
    int return_cnt = -1;

    int pos1 = str1.indexOf(strKeyword);
    int pos2 = str1.indexOf(strKeyword2);
    int offs = strKeyword.length();

    if ((pos1>=0)&&(pos2>=0))
    {
     cnt++;
     if (enum1==variaEnum::eNone)
     {
      zamena[cnt].strBefore = str1;
     }
     else if ((enum1==variaEnum::eNumber)||(enum1==variaEnum::eNumber2))
     {
         zamena[cnt].strBefore = str1.left(pos1 + offs);
         zamena[cnt].strAfter = str1.right(str1.length()-str1.indexOf(strKeyword2)); //strKeyword2.length());
         QString strN = str1.mid(pos1+offs,str1.indexOf(strKeyword2)- pos1-offs);
         int i1 = strN.toInt();
         zamena[cnt].iNumber = i1;
     }
     else if (enum1==variaEnum::ePoints)
     {
         zamena[cnt].strBefore = str1.left(pos1 + offs);
         zamena[cnt].strAfter = str1.right(strKeyword2.length());
         QString strN = str1.mid(pos1+offs,str1.indexOf(strKeyword2)- pos1-offs);
         int x,y;
         QString strX,strY;
         int step = 0;
         for(int i=0;i<strN.length();i++)
         {/*strN.indexOf("(")==i strN.at(i)==')'*/
             if (strN[i]=='(') { step = 1; strX = ""; }
             else if (strN[i]==',') { step = 2; strY = ""; }
             else if (strN[i]==')') { step = 3; x = strX.toInt(); y= strY.toInt(); zamena[cnt].pntsX.append(x);zamena[cnt].pntsY.append(y); }
             else if (step==1) strX += strN[i];
             else if (step==2) strY += strN[i];
         }
     }
     else
     {
         zamena[cnt].strBefore = str1.left(pos1 + offs);
         zamena[cnt].strAfter = str1.right(strKeyword2.length());
         if (str1.indexOf("nil")>0)
         {
             zamena[cnt].strBefore += "\"";
             zamena[cnt].strAfter = "\"" + zamena[cnt].strAfter;
         }

     }
     zamena[cnt].type = enum1;

    return_cnt = cnt;
    }
    else if (enum1==variaEnum::eEmpty)
    {
        cnt++;
        return_cnt = cnt;
    }
    else
        return_cnt = -1;



    return return_cnt;
}

int PropuskLines(QTextStream *stream, QString searchStr, int limit, bool doSave, QString *str1)//пропуск-копирование линий с одним символом поиска
{
    currpos = stream->pos();
    int pos;
    //*str1 = stream->readLine();
    //if (doSave) SaveLines.append(*str1);
    pos = str1->indexOf(searchStr);
    int iBreak = 0;
    while (pos==-1) {
        //ui->textBrowser->append(str1);
        *str1 = stream->readLine();
        if (doSave) SaveLines.append(*str1);
        pos = str1->indexOf(searchStr);
        iBreak++;
        if (iBreak>limit) { stream->seek(currpos+1); return -1; }
    }
    return pos;
}

int PropuskLines(QTextStream *stream, QString searchStr, QString searchStr2, int limit, bool doSave, QString *str1)//пропуск-копирование линий с двумя символами поиска (И то И другое)
{

    currpos = stream->pos();
    int pos, pos2;
    *str1 = stream->readLine();
    if (doSave) SaveLines.append(*str1);
    pos = str1->indexOf(searchStr);
    pos2 = str1->indexOf(searchStr2);
    int iBreak = 0;
    while ((pos==-1)||(pos2==-1)) {
        //ui->textBrowser->append(str1);
        *str1 = stream->readLine();
        if (doSave) SaveLines.append(*str1);
        pos = str1->indexOf(searchStr);
        pos2 = str1->indexOf(searchStr2);
        iBreak++;
        if (iBreak>limit) { stream->seek(currpos+1); return -1; }
    }
    return pos;
}


void Algorythm(QTextStream *stream, bool doTranslitLight)
{
    int pos = 0;
    QString str1,str2;
    QString strErr = "";

    //cfcStepEnum step = cfcStepEnum::eHEAD;



//============ ШАПКА ======================
            // === название PD =====
            QString strN = "";


            if (PropuskLines(stream,"Context",10,true,&strN)==-1)
            {
                strErr = "Слишком много линий при поиске \"Context\"";
                //ui->label->setText(strErr);
                ErrorLines.append(strErr);
                //ui->textBrowser_2->append(strErr);
            }
            pos = strN.indexOf("\\PD/");
            if (pos>0)
            {
                strPDName = strN.right(strN.length()-pos-4);
                //ErrorLines.append(strPDName);
            }
            else
            {
                //ui->label_2->setText("");
                ErrorLines.append("Не удалось определить PD Name");
            }

            str1 = stream->readLine();
            pos = str1.indexOf("\\P/");
            int iBreak = 0;
            while (pos==-1) {
                SaveLines.append(str1);
                //ui->textBrowser->append(str1);
                str1 = stream->readLine();
                pos = str1.indexOf("\\P/");
                iBreak++;
                if (iBreak>55) { /*ui->label->setText("Заголовок слишком длинный");*/ ErrorLines.append("Заголовок слишком длинный"); break; }
            }

//============ПЕРЕЧЕНЬ ПЕРЕМЕННЫХ===================
            QStringList lstVars;

            lineZamena zamena[25];
            int cnt = -1;
            int cnt1 = -1;

            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\I\\V/","  ( )",variaEnum::eAprolName,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\I\\V/","\\active  (",variaEnum::eAprolName,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\I\\V/","\\current  (",variaEnum::eAprolName,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "config"," ,",variaEnum::eNone,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "desc001 = \"","\" ,",variaEnum::eDesc001,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "desc007 = \"","\" ,",variaEnum::eDesc007,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "iec_type = \"","\" ,",variaEnum::eVarType,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "is_output = "," ,",variaEnum::eInputOutput,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "max = "," ,",variaEnum::eMax,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "min = "," ,",variaEnum::eMin,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "msr = \"","\" ,",variaEnum::eMSRno,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "msr = nil"," ,",variaEnum::eNone,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "target = "," ,",variaEnum::eNone,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "tb_structure = "," ,",variaEnum::eNone,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "unit = \"","\" ,",variaEnum::eVelichina,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "var_type"," ,",variaEnum::eNone,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamena);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }

            //bool doTranslitLight = ui->checkBox_3->isChecked();
            QString strR = "";
            for(int i = noVarBegin; i< noVarEnd; i++)//0; i < V->count; i++)
            {
                for(int p = 0; p <= cnt; p++)
                {
                    strR = zamena[p].strBefore;
                    switch(zamena[p].type) {
                    case (variaEnum::eAprolName):
                            strR += V[i].Lines.AprolName;
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eDesc001):
                            strR += Translit(V[i].Lines.DescENG);
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eDesc007):
                            if (doTranslitLight)
                                strR += TranslitLight(V[i].Lines.Desc);
                            else
                                strR += V[i].Lines.Desc;
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eInputOutput):
                            if (V[i].Lines.ShortDesc=="INPUT")
                                strR += "True";
                            else
                                strR += "False";
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eMSRno):
                            strR += V[i].Lines.MSRno;
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eMax):
                            strR += V[i].Lines.Max;
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eMin):
                            strR += V[i].Lines.Min;
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eNone):

                            break;
                    case (variaEnum::ePlcName):
                            strR += V[i].Lines.PlcName;
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eVarType):
                            strR += V[i].Lines.VarType;
                            strR += zamena[p].strAfter;
                            break;
                    case (variaEnum::eVelichina):
                            strR += V[i].Lines.Velichina;
                            strR += zamena[p].strAfter;
                            break;
                    }
                    SaveLines.append(strR);
                    //ui->textBrowser->append(strR);
                }
            }

//===============CFC - WARNING=========================

       QString strT = "";

       /*PropuskLines(&stream,"\\PD/",150,false,&strT);
       SaveLines.append(strT);

       int prer = 0;
       while (PropuskLines(&stream,")",25,true,&strT)!=0) { if (prer++>25) { ui->label->setText("Слишком длинный warning"); break;} }

       PropuskLines(&stream,"\\PD/",5,true,&strT);

       prer = 0;
       while (PropuskLines(&stream,")",50,true,&strT)!=0) { if (prer++>25) { ui->label->setText("Слишком длинный warning2"); break;} }*/

//===============Instance=========================



       if (noVarBegin>0)
       {

         if (PropuskLines(stream,"imported_user =", " ,", 75,true,&strT)==-1)
         {
             strErr = "Слишком много линий при поиске imported_user";
             ErrorLines.append(strErr);
         }

         int cn_break = 0;
         currpos = stream->pos();
         do {
                str1 = stream->readLine();
                pos = str1.indexOf("instance");
                if ((cn_break++)>15) { ErrorLines.append("Слишком много линий при поиске instance"); stream->seek(currpos+1); break; }
         } while(pos<0);

         if (pos>=0)
         {
             pos = str1.indexOf(" ,");
             str1 = str1.insert(pos-1,"_"+QString::number(noPage2Save));
             SaveLines.append(str1);
         }
       }


//===============PAGE=========================

       if (PropuskLines(stream,"\\PD/", "\\C  (", 100,true,&strT)==-1)
       {
           strErr = "Слишком много линий при поиске \"\\PD/\", \"\\C  (\"";
           ErrorLines.append(strErr);
           //ui->label->setText(strErr);
           //ui->textBrowser_2->append(strErr);
       }

       str1 = stream->readLine();
       pos = str1.indexOf(")");
       iBreak = 0;
       while (pos!=0) {
           if (str1.indexOf("height = ")>0)
           {
               int page_size = V->count / KOL_TRDS_PER_PAGE + 1;
               if (page_size>MAX_PAGES_PER_PD)
               {
                   page_size = MAX_PAGES_PER_PD;
                   //V->count = CountVariablesPerTRD_PD()-1;
                   //ui->label->setText("Слишком много переменных > " + QString::number(CountVariablesPerTRD_PD()));
                   //ErrorLines.append("Слишком много переменных > " + QString::number(CountVariablesPerTRD_PD()) + " Будет создано несколько файлов");

               }
               str1 = "   height = " + QString::number(page_size) + " ,";
           }
           SaveLines.append(str1);
           //ui->textBrowser->append(str1);

           str1 = stream->readLine();
           pos = str1.indexOf(")");
           iBreak++;
           if (iBreak>15) { /*ui->label->setText("PAGE слишком длинный");*/ErrorLines.append("PAGE слишком длинный"); break; }
       }

       SaveLines.append(str1);

int i11 = 0;



       if (iTrendsAlarms==0)//TRENDS
       {

//==========TREND - GROUP - TOL===================

    lineZamena zamenaTRD[25];
    cnt = 0;
    cnt1 = -1;


    if (PropuskLines(stream,"AprCcTrend",150,false,&str1)==-1)
    {
        strErr = "Слишком много линий при поиске \"AprCcTrend\"";
        //ui->label->setText(strErr);
        //ui->textBrowser_2->append(strErr);
        ErrorLines.append(strErr);
    }

    QString strAprCcTrend = "AprCcTrend_";
    if (str1.indexOf(strAprCcTrend)<1) strAprCcTrend = "AprCcTrend";
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcTrend,"  (",variaEnum::eNumber,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, "blockno"," ,",variaEnum::eNone,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, "desc"," ,",variaEnum::eNone,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, "fbref"," ,",variaEnum::eNone,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, "link"," ,",variaEnum::eNone,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, "operator"," ,",variaEnum::eNone,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, "pos = "," ,",variaEnum::ePoints,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, "type"," ,",variaEnum::eNone,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eNone,zamenaTRD);
    if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcTrend,"\\I/GROUP  (",variaEnum::eNumber,zamenaTRD);
    if (cnt1!=-1)
    {
        cnt = cnt1; str1 = stream->readLine();
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "value","value",variaEnum::eNone,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    }

    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcTrend,"\\I/TOL  (",variaEnum::eNumber,zamenaTRD);
    if (cnt1!=-1)
    {
        cnt = cnt1; str1 = stream->readLine();
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "value = \"","\" ,",variaEnum::eTolerance,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    }

    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcTrend,"\\I/MAX  (",variaEnum::eNumber,zamenaTRD);
    if (cnt1!=-1)
    {
        cnt = cnt1; str1 = stream->readLine();
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "value = \"","\" ,",variaEnum::eMax,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    }

    //---
    cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcTrend,"\\I/MIN  (",variaEnum::eNumber,zamenaTRD);
    if (cnt1!=-1)
    {
        cnt = cnt1; str1 = stream->readLine();
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "value = \"","\" ,",variaEnum::eMin,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaTRD);
        if (cnt1!=-1) { cnt = cnt1; /*str1 = stream.readLine();*/ }
    }

    strR = "";


    for(int i = noVarBegin; i< noVarEnd; i++)//0; i < V->count; i++)
    {
        i11 = i - noVarBegin;
        for(int p = 1; p <= cnt; p++)
        {
            strR = zamenaTRD[p].strBefore;
            switch(zamenaTRD[p].type) {
            case (variaEnum::eNone):

                    break;
            case (variaEnum::eNumber):
                    strR += QString::number(zamenaTRD[p].iNumber + i11);
                    strR += zamenaTRD[p].strAfter;
                    break;
            case (variaEnum::ePoints):
                    for(int cn=0;cn<zamenaTRD[p].pntsX.count();cn++)
                    {
                       strR += "(" + QString::number(zamenaTRD[p].pntsX[cn]) + "," + QString::number(zamenaTRD[p].pntsY[cn] + YSTEP*i11) + ") ";
                    }
                    strR += zamenaTRD[p].strAfter;
                    break;
            case (variaEnum::eTolerance):
                    strR += V[i].Lines.TOL;
                    strR += zamenaTRD[p].strAfter;
                break;
            case (variaEnum::eMax):
                    strR += V[i].Lines.Max;
                    strR += zamenaTRD[p].strAfter;
                break;
            case (variaEnum::eMin):
                    strR += V[i].Lines.Min;
                    strR += zamenaTRD[p].strAfter;
                break;
            }
            SaveLines.append(strR);
            //ui->textBrowser->append(strR);
        }
    }

       }
       else if (iTrendsAlarms==1)//ALARMS
       {
           //==========ALARM - GROUP - PICTURE===================

               lineZamena zamenaALR[25];
               cnt = 0;
               cnt1 = -1;


               if (PropuskLines(stream,"AprCcAlarmBasic",150,false,&str1)==-1)
               {
                   strErr = "Слишком много линий при поиске \"AprCcAlarmBasic\"";
                   //ui->label->setText(strErr);
                   //ui->textBrowser_2->append(strErr);
                   ErrorLines.append(strErr);
               }

               QString strAprCcAlarmBasic = "AprCcAlarmBasic_";
               if (str1.indexOf(strAprCcAlarmBasic)<1) strAprCcAlarmBasic = "AprCcAlarmBasic";
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcAlarmBasic,"  (",variaEnum::eNumber,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, "blockno"," ,",variaEnum::eNone,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, "desc"," ,",variaEnum::eNone,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, "fbref"," ,",variaEnum::eNone,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, "link"," ,",variaEnum::eNone,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, "operator"," ,",variaEnum::eNone,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, "pos = "," ,",variaEnum::ePoints,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, "type"," ,",variaEnum::eNone,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaALR);
               if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcAlarmBasic,"\\I/ALIAS  (",variaEnum::eNumber,zamenaALR);
               if (cnt1!=-1)
               {
                   cnt = cnt1; str1 = stream->readLine();
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, "value","value",variaEnum::eNone,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcAlarmBasic,"\\I/GROUP  (",variaEnum::eNumber,zamenaALR);
               if (cnt1!=-1)
               {
                   cnt = cnt1; str1 = stream->readLine();
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, "value","value",variaEnum::eNone,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               }
               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcAlarmBasic,"\\I/ON  (",variaEnum::eNumber,zamenaALR);
               if (cnt1!=-1)
               {
                   cnt = cnt1; str1 = stream->readLine();
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, "value","value",variaEnum::eNone,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               }

               //---
               cnt1 = Read2ZamenaByKeyWord(cnt, str1, strAprCcAlarmBasic,"\\I/PICTURE  (",variaEnum::eNumber,zamenaALR);
               if (cnt1!=-1)
               {
                   cnt = cnt1; str1 = stream->readLine();
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, "value = \"","\" ,",variaEnum::eNone,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                   //---
                   cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaALR);
                   if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
               }



               strR = "";
               int i11 = 0;

               for(int i = noVarBegin; i< noVarEnd; i++)//0; i < V->count; i++)
               {
                   i11 = i - noVarBegin;
                   for(int p = 1; p <= cnt; p++)
                   {
                       strR = zamenaALR[p].strBefore;
                       switch(zamenaALR[p].type) {
                       case (variaEnum::eNone):

                               break;
                       case (variaEnum::eNumber):
                               strR += QString::number(zamenaALR[p].iNumber + i11);
                               strR += zamenaALR[p].strAfter;
                               break;
                       case (variaEnum::ePoints):
                               for(int cn=0;cn<zamenaALR[p].pntsX.count();cn++)
                               {
                                  strR += "(" + QString::number(zamenaALR[p].pntsX[cn]) + "," + QString::number(zamenaALR[p].pntsY[cn] + YSTEP*i11) + ") ";
                               }
                               strR += zamenaALR[p].strAfter;
                               break;
                       }
                       SaveLines.append(strR);
                       //ui->textBrowser->append(strR);
                   }
               }

       }
//===========POINTS========================

    lineZamena zamenaPNT[25];
    cnt = -1;
    cnt1 = -1;

    if (PropuskLines(stream,"\\C\\CL",150,false,&str1)==-1)
    {
        strErr = "Слишком много линий при поиске \"\\C\\CL\"";
        //ui->label->setText(strErr);
        //ui->textBrowser_2->append(strErr);
        ErrorLines.append(strErr);
    }

    for(int k=0;k<LINEKOL;k++)
    {
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\C\\CL/","  (",variaEnum::eNumber,zamenaPNT);
        if (cnt1!=-1) {
            cnt = cnt1; str1 = stream->readLine();
        }
        else {
            break;
        }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "points = ( ","  ) ,",variaEnum::ePoints,zamenaPNT);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //---
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaPNT);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        //--
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaPNT);
        if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
    }


    strR = "";

    for(int i = noVarBegin; i< noVarEnd; i++)//0; i < V->count; i++)
    {
        i11 = i - noVarBegin;
        for(int p = 0; p <= cnt; p++)
        {
            strR = zamenaPNT[p].strBefore;
            switch(zamenaPNT[p].type) {
            case (variaEnum::eNone):

                    break;
            case (variaEnum::eNumber):
                    strR += QString::number(zamenaPNT[p].iNumber + i11*LINEKOL);
                    strR += zamenaPNT[p].strAfter;
                    break;
            case (variaEnum::ePoints):
                    for(int cn=0;cn<zamenaPNT[p].pntsX.count();cn++)
                    {
                       strR += "(" + QString::number(zamenaPNT[p].pntsX[cn]) + "," + QString::number(zamenaPNT[p].pntsY[cn] + YSTEP*i11) + ") ";
                    }
                    strR += zamenaPNT[p].strAfter;
                    break;
            case (variaEnum::eAprolName):
                    strR += V[i].Lines.AprolName;
                    strR += zamenaPNT[p].strAfter;
                    break;
            case (variaEnum::eVarType):
                    strR += V[i].Lines.VarType;
                    strR += zamenaPNT[p].strAfter;
                    break;
            }
            SaveLines.append(strR);
            //ui->textBrowser->append(strR);
        }
    }


    //===========VARIABLES========================

        lineZamena zamenaVAR[125];
        cnt = -1;
        cnt1 = -1;

        if (str1.indexOf("\\C\\IB")==-1)
        {
            if (PropuskLines(stream,"\\C\\IB",150,false,&str1)==-1)
            {
                strErr = "Слишком много линий при поиске \"\\C\\IB\"";
                //ui->label->setText(strErr);
                //ui->textBrowser_2->append(strErr);
                ErrorLines.append(strErr);
            }
        }

        //--- VAR
        cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\C\\IB/","  (",variaEnum::eNumber,zamenaVAR);
        if (cnt1!=-1)
        {
            cnt = cnt1; str1 = stream->readLine();
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "iec_type = \"","\" ,",variaEnum::eVarType,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "pos = "," ,",variaEnum::ePoints,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\I\\V/"," ,",variaEnum::eAprolName,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "width","width",variaEnum::eNone,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        }
        //-------------------
        //--- UNIT - DESC - MSR - MIN - MAX
        for(int k=0;k<5;k++)
        {
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\C\\IB/","  (",variaEnum::eNumber,zamenaVAR);
            if (cnt1!=-1) {
                cnt = cnt1; str1 = stream->readLine();
            }
            else {
                break;
            }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "ext_mode","ext_mode",variaEnum::eNone,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //---
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "iec_type","iec_type",variaEnum::eNone,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "pos = "," ,",variaEnum::ePoints,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\I\\V/"," ,",variaEnum::eAprolName,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "width","width",variaEnum::eNone,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
            //--
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaVAR);
            if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
        }
        //-------------------

        strR = "";

        for(int i = noVarBegin; i< noVarEnd; i++)//0; i < V->count; i++)
        {
            i11 = i - noVarBegin;
            for(int p = 0; p <= cnt; p++)
            {
                strR = zamenaVAR[p].strBefore;
                switch(zamenaVAR[p].type) {
                case (variaEnum::eNone):

                        break;
                case (variaEnum::eNumber):
                        strR += QString::number(zamenaVAR[p].iNumber + i11*LINEKOL);
                        strR += zamenaVAR[p].strAfter;
                        break;
                case (variaEnum::ePoints):
                        for(int cn=0;cn<zamenaVAR[p].pntsX.count();cn++)
                        {
                           strR += "(" + QString::number(zamenaVAR[p].pntsX[cn]) + "," + QString::number(zamenaVAR[p].pntsY[cn] + YSTEP*i11) + ") ";
                        }
                        strR += zamenaVAR[p].strAfter;
                        break;
                case (variaEnum::eAprolName):
                        strR += V[i].Lines.AprolName;
                        strR += zamenaVAR[p].strAfter;
                        break;
                case (variaEnum::eVarType):
                        strR += V[i].Lines.VarType;
                        strR += zamenaVAR[p].strAfter;
                        break;
                }
                SaveLines.append(strR);
                //ui->textBrowser->append(strR);
            }
        }

        //============= COMMENT ===============
        bool doComment = false;

        doComment = (str1.indexOf("\\C\\TC/")>0);

        if (!doComment) doComment = (PropuskLines(stream,"\\C\\TC/",5,true,&strT)>0);

        if (doComment)
        {
            lineZamena zamenaCOMM[25];
            cnt = -1;
            cnt1 = -1;

            //if (str1.indexOf("\\C\\TC")==-1)
            //    PropuskLines(&stream,"\\C\\TC",150,false,&str1);

            //--- VAR
            cnt1 = Read2ZamenaByKeyWord(cnt, str1, "\\C\\TC/","  (",variaEnum::eNumber,zamenaCOMM);
            if (cnt1!=-1)
            {
                cnt = cnt1; str1 = stream->readLine();
                //---
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "alignment","alignment",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "bgColor","bgColor",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "bgTransp","bgTransp",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "border","border",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "border","border",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "family","family",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "Color","Color",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "orientation","orientation",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "pos = "," ,",variaEnum::ePoints,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "size = (",",",variaEnum::eNumber2,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "source = \"","\" ,",variaEnum::eDesc007,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "style","style",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "type","type",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "weight","weight",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "word","word",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, ")",")",variaEnum::eNone,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; str1 = stream->readLine(); }
                //--
                cnt1 = Read2ZamenaByKeyWord(cnt, str1, "","",variaEnum::eEmpty,zamenaCOMM);
                if (cnt1!=-1) { cnt = cnt1; /*str1 = stream.readLine();*/ }
                //-------------------
            }

            strR = "";
            QString strDescr = "";

            for(int i = noVarBegin; i< noVarEnd; i++)//0; i < V->count; i++)
            {
                i11 = i - noVarBegin;
                strDescr = V[i].Lines.MSRno + " " + V[i].Lines.Desc;
                for(int p = 0; p <= cnt; p++)
                {
                    strR = zamenaCOMM[p].strBefore;
                    switch(zamenaCOMM[p].type) {
                    case (variaEnum::eNone):

                            break;
                    case (variaEnum::eNumber):
                            strR += QString::number(zamenaCOMM[p].iNumber + i11);
                            strR += zamenaCOMM[p].strAfter;
                            break;
                    case (variaEnum::eNumber2):
                            strR += QString::number(strDescr.length());
                            strR += zamenaCOMM[p].strAfter;
                            break;
                    case (variaEnum::ePoints):
                            for(int cn=0;cn<zamenaCOMM[p].pntsX.count();cn++)
                            {
                               strR += "(" + QString::number(zamenaCOMM[p].pntsX[cn]) + "," + QString::number(zamenaCOMM[p].pntsY[cn] + YSTEP*i11) + ") ";
                            }
                            strR += zamenaCOMM[p].strAfter;
                            break;
                    case (variaEnum::eDesc007):
                            strR += strDescr;
                            strR += zamenaCOMM[p].strAfter;
                            break;

                    }
                    SaveLines.append(strR);
                    //ui->textBrowser->append(strR);
                }
            }
        }

    //==========

        strT = "";

        if (str1.indexOf("DOC  ( )")==-1)
        {
            if (PropuskLines(stream,"DOC  ( )",350,false,&strT)==-1)
            {
                strErr = "Слишком много линий при поиске \"DOC  ( )\"";
                //ui->label->setText(strErr);
                //ui->textBrowser_2->append(strErr);
                ErrorLines.append(strErr);
            }
            SaveLines.append(strT);
        }
        else
        {
            SaveLines.append(str1);
        }

        while(!stream->atEnd())
        {
            str1 = stream->readLine();
            SaveLines.append(str1);
        }
}

void MainWindow::on_pushButton_5_clicked()//open IMP
{
    QString settingsFile = "settings.apr";
        //читаем из файла настроек последний открытый путь
        QFile fiS(settingsFile);
        QString pathI = "";
        QString pathC = "";
        if (fiS.open(QIODevice::ReadOnly))
        {
            QTextStream streamIS;
            streamIS.setCodec("UTF-8");
            streamIS.setDevice(&fiS);
            if (!streamIS.atEnd())
                      pathI = streamIS.readLine();//первая запись - для imp
            if (!streamIS.atEnd())
                      pathC = streamIS.readLine();
            fiS.close();
        }





    //SaveLines.clear();
    ui->textBrowser_2->clear();
    //==проверяем номера переменных для обработки===
    noVarBegin = ui->spinBox->value();
    noVarEnd = ui->spinBox_2->value();
    if (noVarEnd>V->count-1) noVarEnd = V->count;
    if ((noVarBegin>=noVarEnd)||(noVarEnd-noVarBegin>CountVariablesPerTRD_PD())) noVarBegin = noVarEnd - CountVariablesPerTRD_PD();
    if (noVarBegin<0) noVarBegin = 0;
    if (noVarEnd<1) noVarEnd = 1;
    ui->spinBox->setValue(noVarBegin);
    ui->spinBox_2->setValue(noVarEnd);

    //==== поехали=========


    QString ImpFile = "";
    ImpFile = QFileDialog::getOpenFileName(this, tr("Выберите экспортированный IMP-файл"), pathI, "*.imp");
    if (ImpFile.length()<3) return;

        //ui->textBrowser->clear();

    QFile fi(ImpFile);
    if (!fi.open(QIODevice::ReadOnly)) return;

    QTextStream stream;
    stream.setCodec("UTF-8");
    stream.setDevice(&fi);

    bool doTrans = ui->checkBox_3->isChecked();

    int cn_break = 0;

    while (true)
    {
        cn_break++;
        if (cn_break>100) {ui->textBrowser_2->append("Слишком много итераций..."); break; }
        ui->textBrowser_2->append(QString::number(cn_break));

        stream.seek(0);
        SaveLines.clear();

        Algorythm(&stream, doTrans);

        //======================================

        ui->textBrowser->clear();
        int posPage = -1;
        for(int i=0;i<SaveLines.count();i++)
        {
          //заменяем PD name, если страничка не первая
            if (noPage2Save>0)
            {
                posPage = SaveLines[i].indexOf(strPDName);
                if (posPage>0) SaveLines[i].insert(posPage+strPDName.length(),"_"+QString::number(noPage2Save));
            }
            //ui->textBrowser->append(SaveLines[i]);
        }
        //ui->textBrowser->append("#Обработано переменных от "+QString::number(noVarBegin) + " до "+QString::number(noVarEnd));
        for(int i=0;i<ErrorLines.count();i++)
        {
            ui->textBrowser_2->append(ErrorLines[i]);
        }
        ui->label_2->setText(strPDName);

        //сохраняем автоматически в файл
        int slashpos = ImpFile.lastIndexOf(".imp");
        QString SaveFile = ImpFile.left(slashpos);
        SaveFile += "_" + QString::number(noPage2Save) + ".imp";

        if (SaveFile.length()>3)
        {
            QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
            QFile fout(SaveFile);
            if (!fout.open(QIODevice::WriteOnly)) return;
            QTextStream stream(&fout);
            for(int i=0; i < SaveLines.count(); i++)
            {
                stream << SaveLines.value(i);
                stream << "\n";
            }
            fout.close();
           //ui->label->setText("Сохранено переменных от "+QString::number(noVarBegin) + " до "+QString::number(noVarEnd));
            ui->textBrowser_2->append("Сохранено переменных от "+QString::number(noVarBegin) + " до "+QString::number(noVarEnd));
            ui->textBrowser_2->append("Сохранено в файл: " + SaveFile);
        }


        if (noVarEnd<V->count-1)//автоматически делаем вторую страницу
        {
            noVarBegin = noVarEnd;
            noVarEnd+=CountVariablesPerTRD_PD();
            if (noVarEnd>V->count) noVarEnd = V->count;
            ui->spinBox->setValue(noVarBegin);
            ui->spinBox_2->setValue(noVarEnd);
            noPage2Save++;
        }
        else {
            break;
        }

        //if (noPage2Save!=0) ui->textBrowser_2->append(strPDName + "_" + QString::number(noPage2Save));
    }//while

    for(int i=0;i<SaveLines.count();i++)
    {
         ui->textBrowser->append(SaveLines[i]);
    }

    fi.close();



    //записываем в файл настроек последний открытый путь
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QFile foS(settingsFile);
    if (!foS.open(QIODevice::WriteOnly)) return;
    QTextStream streamOS(&foS);
    QString path = ImpFile.left(ImpFile.lastIndexOf("/"));
    streamOS << path;
    streamOS << "\n";
    streamOS << pathC;
    streamOS << "\n";
    foS.close();

}



void MainWindow::on_pushButton_4_clicked()
{
    //if (ui->checkBox_3->isChecked())
    {
        QString SaveFile = "";
        SaveFile = QFileDialog::getSaveFileName(this, tr("Введите имя файла"), "", "*.imp");
        if (SaveFile.length()>3)
        {
            QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
            QFile fout(SaveFile);
            if (!fout.open(QIODevice::WriteOnly)) return;
            QTextStream stream(&fout);
            for(int i=0; i < SaveLines.count(); i++)
            {
                stream << SaveLines.value(i);
                stream << "\n";
            }
            fout.close();
            ui->label->setText("Сохранено переменных от "+QString::number(noVarBegin) + " до "+QString::number(noVarEnd));
        }
    }//if check
}

void MainWindow::on_radioButton_clicked()
{
    if (ui->radioButton->isChecked())
    {
        iTrendsAlarms = 0;
    } else if (ui->radioButton_2->isChecked())
    {
        iTrendsAlarms = 1;
    }
}

void MainWindow::on_radioButton_2_clicked()
{
    if (ui->radioButton->isChecked())
    {
        iTrendsAlarms = 0;
    } else if (ui->radioButton_2->isChecked())
    {
        iTrendsAlarms = 1;
    }
}
