#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFile>
#include <QtWidgets>
#include <QtGui>

//=================================================================
#define MAX_STR_CNT 10000

static QString CsvFile;        /*файл-таблица сигналов*/
static bool isCSVchoosed;

static QString ImpFile;        /*файл-шаблона импорта сигналов в апрол*/
static bool isIMPchoosed;
#define SHABLON_SIZE 50
#define HEADER_SIZE_MAX 50
static QString Shablon[SHABLON_SIZE];    /*сам шаблон*/
static int shablon_p;          /*кол-во строк в шаблоне*/
static int zamenaLevel = 0;

#define STANDART_AI_SIZE 12
static QString ShablonStandartAI[STANDART_AI_SIZE] = {"",
                                                      "   remPv = \"APROLKEY2\" ,",
                                                      "   type = \"APROLKEY3\" ,",
                                                      "   mode = \"APROLKEY4\" ,",
                                                      "   desc007 = \"APROLKEY5\" ,",
                                                      "   unit = \"APROLKEY6\",",
                                                      "   msr = \"APROLKEY7\",",
                                                      "   min = \"APROLKEY8\",",
                                                      "   max = \"APROLKEY9\",",
                                                      "   desc001 = \"APROLKEY0\" ,",
                                 ")",
                                 ""};
#define STANDART_AI_SIZE_TOL 13
static QString ShablonStandartAITOL[STANDART_AI_SIZE_TOL] = {"",
                                                      "   remPv = \"APROLKEY2\" ,",
                                                      "   type = \"APROLKEY3\" ,",
                                                      "   mode = \"APROLKEY4\" ,",
                                                      "   desc007 = \"APROLKEY5\" ,",
                                                      "   unit = \"APROLKEY6\",",
                                                      "   msr = \"APROLKEY7\",",
                                                      "   min = \"APROLKEY8\",",
                                                      "   max = \"APROLKEY9\",",
                                                      "   tol = \"APROLKEY10\",",
                                                      "   desc001 = \"APROLKEY0\" ,",
                                 ")",
                                 ""};

static QString Header[SHABLON_SIZE];
static int headerSize = 0;

static QStringList SaveLines;  /*результат для сохранения в файл*/

static int V_p;
struct varia{
    QString AprolName;  /*Name*/
    QString PlcName;
    QString VarType;
    QString ShortDesc;  /*INPUT-OUTPUT*/
    QString Desc;       /*Description RUS*/
    QString Velichina;  /*Unit*/
    QString MSRno;      /*MSR No*/
    QString Min;        /*MIN*/
    QString Max;        /*MAX*/
    QString Tol;
    QString Desc001;    /*Description ENG*/
};
static class vari{
public:
    varia Lines;
    void operator=(QString csv_line);
} V[MAX_STR_CNT];

QString Z[MAX_STR_CNT][11];
static int cntZ = 0;
//---
void vari::operator =(QString csv_line)
{
    QStringList list1 = csv_line.split(QRegExp("[;]"));
    this->Lines.AprolName = list1.value(0);
    this->Lines.PlcName   = list1.value(1);
    this->Lines.VarType   = list1.value(2);
    this->Lines.ShortDesc = list1.value(3);
    if (this->Lines.ShortDesc.length()<1) this->Lines.ShortDesc = "OUTPUT";
    this->Lines.Desc      = list1.value(4);
    this->Lines.Velichina = list1.value(5);
    this->Lines.MSRno     = list1.value(6);
    this->Lines.Min       = list1.value(7);
    this->Lines.Max       = list1.value(8);
    this->Lines.Tol       = list1.value(9);
    this->Lines.Desc001   = list1.value(10);
}

QString ZamenaLight(QString strin)
{
    QString strout = "";
    QString str = "";

    for(int i = 0; i < strin.length(); i++)
    {
        str = strin[i];

        if (str=="у") strout += "y";
        else if (str=="х") strout += "x";
        else if (str=="Е") strout += "E";
        else if (str=="Ё") strout += "E";
        else if (str=="З") strout += "3";
        else if (str=="Р") strout += "P";
        else if (str=="С") strout += "C";
        else if (str=="Т") strout += "T";
        else if (str=="У") strout += "У";
        else if (str=="Х") strout += "X";
        else if (str=="ё") strout += "e";
        else strout += str;

    }
    if (strout.length()<1) strout=" ";
    return strout;
}

QString Zamena(QString strin)
{
    QString strout = "";
    QString str = "";

    for(int i = 0; i < strin.length(); i++)
    {
        str = strin[i];

        if (str=="а") strout += "a";
        else if (str=="г") strout += "r";
        else if (str=="е") strout += "e";
        else if (str=="и") strout += "u";
        else if (str=="о") strout += "o";
        else if (str=="п") strout += "n";
        else if (str=="р") strout += "p";
        else if (str=="с") strout += "c";
        else if (str=="т") strout += "m";
        else if (str=="у") strout += "y";
        else if (str=="х") strout += "x";
        else if (str=="А") strout += "A";
        else if (str=="В") strout += "B";
        else if (str=="Е") strout += "E";
        else if (str=="Ё") strout += "E";
        else if (str=="З") strout += "3";
        else if (str=="К") strout += "K";
        else if (str=="М") strout += "M";
        else if (str=="Н") strout += "H";
        else if (str=="О") strout += "O";
        else if (str=="Р") strout += "P";
        else if (str=="С") strout += "C";
        else if (str=="Т") strout += "T";
        else if (str=="У") strout += "У";
        else if (str=="Х") strout += "X";
        else if (str=="ё") strout += "e";
        else strout += str;

    }
    if (strout.length()<1) strout=" ";
    return strout;
}

QString ZamenaPlus(QString strin)
{
    QString strout = "";
    QString str = "";

    for(int i = 0; i < strin.length(); i++)
    {
        str = strin[i];

        if (str=="а") strout += "a";
        else if (str=="г") strout += "r";
        else if (str=="е") strout += "e";
        else if (str=="и") strout += "u";
        else if (str=="й") strout += "u";
        else if (str=="о") strout += "o";
        else if (str=="п") strout += "n";
        else if (str=="р") strout += "p";
        else if (str=="с") strout += "c";
        else if (str=="т") strout += "m";
        else if (str=="у") strout += "y";
        else if (str=="х") strout += "x";
        else if (str=="А") strout += "A";
        else if (str=="В") strout += "B";
        else if (str=="Е") strout += "E";
        else if (str=="Ё") strout += "E";
        else if (str=="З") strout += "3";
        else if (str=="К") strout += "K";
        else if (str=="М") strout += "M";
        else if (str=="Н") strout += "H";
        else if (str=="О") strout += "O";
        else if (str=="Р") strout += "P";
        else if (str=="С") strout += "C";
        else if (str=="Т") strout += "T";
        else if (str=="У") strout += "У";
        else if (str=="Х") strout += "X";
        else if (str=="ё") strout += "e";
        else strout += str;

    }
    if (strout.length()<1) strout=" ";
    return strout;
}

QString HardZamena(QString strin)
{
    QString strout = "";
    QString str = "";

    for(int i = 0; i < strin.length(); i++)
    {
        str = strin[i];

        if (str=="а") strout += "a";
        else if (str=="б") strout += "b";
        else if (str=="в") strout += "v";
        else if (str=="г") strout += "g";
        else if (str=="д") strout += "d";
        else if (str=="е") strout += "e";
        else if (str=="ё") strout += "e";
        else if (str=="ж") strout += "z";
        else if (str=="з") strout += "z";
        else if (str=="и") strout += "i";
        else if (str=="й") strout += "i";
        else if (str=="к") strout += "k";
        else if (str=="л") strout += "l";
        else if (str=="м") strout += "m";
        else if (str=="н") strout += "n";
        else if (str=="о") strout += "o";
        else if (str=="п") strout += "p";
        else if (str=="р") strout += "r";
        else if (str=="с") strout += "s";
        else if (str=="т") strout += "t";
        else if (str=="у") strout += "u";
        else if (str=="ф") strout += "f";
        else if (str=="х") strout += "h";
        else if (str=="ц") strout += "c";
        else if (str=="ч") strout += "c";
        else if (str=="ш") strout += "s";
        else if (str=="щ") strout += "s";
        else if (str=="ъ") strout += " ";
        else if (str=="ы") strout += "y";
        else if (str=="ь") strout += " ";
        else if (str=="э") strout += "e";
        else if (str=="ю") strout += "u";
        else if (str=="я") strout += "a";
        else if (str=="А") strout += "A";
        else if (str=="Б") strout += "B";
        else if (str=="В") strout += "V";
        else if (str=="Г") strout += "G";
        else if (str=="Д") strout += "D";
        else if (str=="Е") strout += "E";
        else if (str=="Ё") strout += "E";
        else if (str=="Ж") strout += "Z";
        else if (str=="З") strout += "Z";
        else if (str=="И") strout += "I";
        else if (str=="Й") strout += "I";
        else if (str=="К") strout += "K";
        else if (str=="М") strout += "L";
        else if (str=="М") strout += "M";
        else if (str=="Н") strout += "N";
        else if (str=="О") strout += "O";
        else if (str=="П") strout += "P";
        else if (str=="Р") strout += "R";
        else if (str=="С") strout += "C";
        else if (str=="Т") strout += "T";
        else if (str=="У") strout += "U";
        else if (str=="Ф") strout += "F";
        else if (str=="Х") strout += "H";
        else if (str=="Ц") strout += "C";
        else if (str=="Ч") strout += "C";
        else if (str=="Ш") strout += "S";
        else if (str=="Щ") strout += "S";
        else if (str=="Ъ") strout += " ";
        else if (str=="Ы") strout += "Y";
        else if (str=="Ь") strout += " ";
        else if (str=="Э") strout += "E";
        else if (str=="Ю") strout += "U";
        else if (str=="Я") strout += "A";
        else strout += str;

    }
    if (strout.length()<1) strout=" ";
    return strout;
}

bool IsThereCyrillic(QString strin)
{
    QString str = "";

    for(int i = 0; i < strin.length(); i++)
    {
        str = strin[i];

        if (str=="а") return true;
        else if (str=="б") return true;
        else if (str=="в") return true;
        else if (str=="г") return true;
        else if (str=="д") return true;
        else if (str=="е") return true;
        else if (str=="ё") return true;
        else if (str=="ж") return true;
        else if (str=="з") return true;
        else if (str=="и") return true;
        else if (str=="й") return true;
        else if (str=="к") return true;
        else if (str=="л") return true;
        else if (str=="м") return true;
        else if (str=="н") return true;
        else if (str=="о") return true;
        else if (str=="п") return true;
        else if (str=="р") return true;
        else if (str=="с") return true;
        else if (str=="т") return true;
        else if (str=="у") return true;
        else if (str=="ф") return true;
        else if (str=="х") return true;
        else if (str=="ц") return true;
        else if (str=="ч") return true;
        else if (str=="ш") return true;
        else if (str=="щ") return true;
        else if (str=="ъ") return true;
        else if (str=="ы") return true;
        else if (str=="ь") return true;
        else if (str=="э") return true;
        else if (str=="ю") return true;
        else if (str=="я") return true;
        else if (str=="А") return true;
        else if (str=="Б") return true;
        else if (str=="В") return true;
        else if (str=="Г") return true;
        else if (str=="Д") return true;
        else if (str=="Е") return true;
        else if (str=="Ё") return true;
        else if (str=="Ж") return true;
        else if (str=="З") return true;
        else if (str=="И") return true;
        else if (str=="Й") return true;
        else if (str=="К") return true;
        else if (str=="М") return true;
        else if (str=="М") return true;
        else if (str=="Н") return true;
        else if (str=="О") return true;
        else if (str=="П") return true;
        else if (str=="Р") return true;
        else if (str=="С") return true;
        else if (str=="Т") return true;
        else if (str=="У") return true;
        else if (str=="Ф") return true;
        else if (str=="Х") return true;
        else if (str=="Ц") return true;
        else if (str=="Ч") return true;
        else if (str=="Ш") return true;
        else if (str=="Щ") return true;
        else if (str=="Ъ") return true;
        else if (str=="Ы") return true;
        else if (str=="Ь") return true;
        else if (str=="Э") return true;
        else if (str=="Ю") return true;
        else if (str=="Я") return true;
        else return false;
    }
    return false;
}

QString RemoveSpec(QString strin)
{
    QString strout = "";
    QString str = "";

    for(int i = 0; i < strin.length(); i++)
    {
        str = strin[i];

        if (str=="\"") ;
        else strout += str;

    }
    if (strout.length()<1) strout=" ";
    return strout;
}


//================================================================

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    isCSVchoosed = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}




void MainWindow::on_pushButton_clicked()/*открываем CSV*/
{

        QString settingsFile = "settings.apr";
            //читаем из файла настроек последний открытый путь
            QFile fiS(settingsFile);
            QString pathI = "";
            QString pathC = "";
            if (fiS.open(QIODevice::ReadOnly))
            {
                QTextStream streamIS;
                streamIS.setCodec("UTF-8");
                streamIS.setDevice(&fiS);
                if (!streamIS.atEnd())
                          pathI = streamIS.readLine();
                if (!streamIS.atEnd())
                          pathC = streamIS.readLine();//вторая запись - для csv
                fiS.close();
            }



        CsvFile = "";
        CsvFile = QFileDialog::getOpenFileName(this, tr("Выберите CSV-файл"), pathC, "*.csv");
        if (CsvFile.length()>3) isCSVchoosed = true; else { isCSVchoosed = false; return; }


        ui->checkBox->setChecked(isCSVchoosed);
        ui->textBrowser->clear();

        QFile fi(CsvFile);
        if (!fi.open(QIODevice::ReadOnly)) return;
        QTextStream stream;
        QString str1;
        bool isLongDesc = false;
        //stream.setCodec("UTF-8");
        stream.setCodec(QTextCodec::codecForName("UTF-8"));
        stream.setDevice(&fi);
        V_p = 0;
        if (ui->checkBox_4->isChecked()) str1 = stream.readLine();/*пропустить первую строку CSV-файла*/
        while (!stream.atEnd())
        {
            str1 = stream.readLine();

            str1 = RemoveSpec(str1);

            V[V_p] = str1;
            if (V[V_p].Lines.AprolName.length()>1)
            {
                ui->textBrowser->append(str1);

                if (IsThereCyrillic(V[V_p].Lines.Desc))//проверка русских дескрипшенов на длину
                {
                    int lenD = V[V_p].Lines.Desc.length();
                    if ( lenD > 32)
                    {
                        ui->textBrowser->append("");
                        ui->textBrowser->append("============>>  ДЛИННОВАТАЯ СТРОКА (>32) ! = " +  QString::number(lenD) + " символов, удалите " + QString::number(lenD - 32));
                        ui->textBrowser->append("");
                        isLongDesc = true;
                    }
                }
                V_p++;
            }
            //else
            //    str1="";
        }
        fi.close();

        if (isLongDesc) ui->textBrowser->append("===== ГДЕ-ТО ДЛИННОВАТАЯ СТРОКА ! =====");

        //записываем в файл настроек последний открытый путь
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
        QFile foS(settingsFile);
        if (!foS.open(QIODevice::WriteOnly)) return;
        QTextStream streamOS(&foS);
        streamOS << pathI;
        streamOS << "\n";
        QString path = CsvFile.left(CsvFile.lastIndexOf("/"));
        streamOS << path;
        streamOS << "\n";

        foS.close();
}



void MainWindow::on_pushButton_5_clicked()//жми на газ
{
    ui->textBrowser->clear();

    QString newTask = ui->comboBox->currentText();
    if (newTask.length()>2)
    {
        for(int i = 0; i < shablon_p; i++)
        {
            QString str1 = Shablon[i];
            int posTASK = str1.indexOf("\\TASK/");//поиск названий тасков
            if (posTASK > 0)
            {
                int posTASK1 = str1.indexOf(("  ("));
                if (posTASK1>posTASK)
                {
                    QString strDo = str1.left(posTASK + 6);
                    QString str2 = str1.mid(posTASK + 6, posTASK1 - posTASK - 6);
                    int posT = str2.indexOf("\\");
                    if (posT>-1)
                    {
                        QString strPosle = str1.mid(posTASK + 6 + posT, str1.length());
                        if (newTask=="GLOBAL")
                            Shablon[i] = str1.left(posTASK) + strPosle;
                        else
                            Shablon[i] = strDo + newTask + strPosle;
                    }
                }
            }
        }
        if (newTask=="GLOBAL")
        {
            headerSize--;
        }
        else
        {
            for(int i = 0; i < headerSize; i++)
            {
                QString str1 = Header[i];
                int posTASK = str1.indexOf("\\TASK/");//поиск названий тасков
                if (posTASK > 0)
                {
                    int posTASK1 = str1.indexOf(("  ("));
                    if (posTASK1>posTASK)
                    {
                        QString strDo = str1.left(posTASK + 6);
                        QString str2 = str1.mid(posTASK + 6, str1.length() - posTASK - 6);
                        int posT = str2.indexOf("  (");
                        if (posT>-1)
                        {
                            QString strPosle = str2.mid(posT, str2.length()-posT);
                            Header[i] = strDo + newTask + strPosle;
                        }
                    }
                }
            }
        }
    }


    //смотрим введены ли какие-нибудь конкретные номера сигналов
    QString strSignalNumbers = ui->lineEdit_2->text();

    QList<int>numlist;
    QStringList listS1 = strSignalNumbers.split(QRegExp("[-]"));
    for(int i1=0;i1<listS1.count();i1++)
    {

        QStringList listSignalNumbers = listS1[i1].split(QRegExp("[;, ]"));

        if (numlist.count()>0)
        {
            for(int p=numlist.last()+1;p<listSignalNumbers[0].toInt();p++)
                numlist<<p;
        }

        int usercount1 = listSignalNumbers.count();
        //int numbers[usercount];
        for(int i=0;i<usercount1;i++)
        {
            numlist << listSignalNumbers[i].toInt();
        }

    }
    int usercount = numlist.count();

    if ( (strSignalNumbers=="Все") ||  (strSignalNumbers=="") )
        usercount = 0;


        if (ui->checkBox->isChecked() && ui->checkBox_2->isChecked())
        {
            ui->checkBox->setChecked(false); ui->checkBox_2->setChecked(false);
            QString APROLKEY = ui->lineEdit->text();

            if (headerSize>0)
            {
                for(int i=0;i<headerSize;i++)
                {

                    ui->textBrowser->append(Header[i]);
                    SaveLines.append(Header[i]);
                }
            }

            int num1 = 0, num2 = 0, num3 = 0;
            QString newline[20];
            for(int p = 0; p < V_p; p++)
            {
                if (usercount>0)
                {
                    bool yes = false;
                    for(int iuc=0;iuc<usercount;iuc++)
                    {
                        if (p==numlist[iuc])
                        {
                            yes = true;
                            break;
                        }
                    }
                    if (!yes) continue;
                }

                for(int i = 0; i <= shablon_p; i++)
                {
                    num1 = Shablon[i].indexOf(APROLKEY);
                    if (num1 > 0)
                    {
                        num2 = num1 + APROLKEY.length();
                        num3 = Shablon[i].mid(num2,1).toInt();
                        newline[i] = Shablon[i].mid(0,num1);
                        switch (num3) {
                            case 1: newline[i] += V[p].Lines.AprolName; break;
                            case 2: newline[i] += V[p].Lines.PlcName; break;
                            case 3: newline[i] += V[p].Lines.VarType; break;
                            case 4: newline[i] += V[p].Lines.ShortDesc; break;
                            case 0:
                                    switch (zamenaLevel) {
                                    case 0:
                                        newline[i] +=  V[p].Lines.Desc001;
                                    break;
                                    case 1:
                                        newline[i] +=  ZamenaLight( V[p].Lines.Desc001);
                                    break;
                                    case 2:
                                        newline[i] += Zamena(V[p].Lines.Desc001);
                                    break;
                                    case 3:
                                        newline[i] += ZamenaPlus(V[p].Lines.Desc001);
                                    break;
                                    case 4:
                                        newline[i] += HardZamena(V[p].Lines.Desc001);
                                    break;
                                    }
                            break;
                            case 5:
                                    switch (zamenaLevel) {
                                    case 0:
                                        newline[i] +=  V[p].Lines.Desc;
                                    break;
                                    case 1:
                                        newline[i] +=  ZamenaLight( V[p].Lines.Desc);
                                    break;
                                    case 2:
                                        newline[i] += Zamena(V[p].Lines.Desc);
                                    break;
                                    case 3:
                                        newline[i] += ZamenaPlus(V[p].Lines.Desc);
                                    break;
                                    case 4:
                                        newline[i] += HardZamena(V[p].Lines.Desc);
                                    break;
                                    }

                            break;
                            case 6: newline[i] += V[p].Lines.Velichina; break;
                            case 7: newline[i] += V[p].Lines.MSRno; break;
                            case 8: newline[i] += V[p].Lines.Min; break;
                            case 9: newline[i] += V[p].Lines.Max; break;
                        }//switch
                        newline[i] += Shablon[i].mid(num2+1, Shablon[i].length()-num2);
                        ui->textBrowser->append(newline[i]);
                        SaveLines.append(newline[i]);
                    }//if num1 > 0
                    else
                    {
                        ui->textBrowser->append(Shablon[i]);
                        SaveLines.append(Shablon[i]);
                    }
                }//for i
            }//for p
            ui->checkBox_3->setChecked(true);
        }//if checked
        else ui->checkBox_3->setChecked(false);
}




void MainWindow::on_pushButton_4_clicked()//открыть экспорт ИМП
{
    QString settingsFile = "settings.apr";
    bool isGlobalFound = false;
        //читаем из файла настроек последний открытый путь
        QFile fiS(settingsFile);
        QString pathI = "";
        QString pathC = "";
        if (fiS.open(QIODevice::ReadOnly))
        {
            QTextStream streamIS;
            streamIS.setCodec("UTF-8");
            streamIS.setDevice(&fiS);
            if (!streamIS.atEnd())
                      pathI = streamIS.readLine();//первая запись - для imp
            if (!streamIS.atEnd())
                      pathC = streamIS.readLine();
            fiS.close();
        }



        ImpFile = "";
            ImpFile = QFileDialog::getOpenFileName(this, tr("Выберите экспортированный IMP-файл"), pathI, "*.imp");
            if (ImpFile.length()<3) return;

            for(int iz=0;iz<MAX_STR_CNT;iz++)
                for(int pz=0;pz<11;pz++)
                    Z[iz][pz] = "";

            ui->textBrowser->clear();
            ui->comboBox->clear();
            ui->comboBox->addItem("GLOBAL");

            QFile fi(ImpFile);
            if (!fi.open(QIODevice::ReadOnly)) return;
            QTextStream stream;
            QString str1,str2;
            stream.setCodec("UTF-8");
            stream.setDevice(&fi);
            int pos = 0;
            int posTASK, posTASK1;
            bool gotHeader = false;
            headerSize = 0;
            int zPnt = -1;
            int zPos = -1;

            while (!stream.atEnd())
            {
                str1 = stream.readLine();
                pos = str1.indexOf("\\VAR/");
                if (!gotHeader)
                {
                    if (pos>1)
                    {
                        str2 = str1.left(pos+5) + "APROLKEY1" + "  (";
                        ui->textBrowser->append(str2); Shablon[0] = str2;
                        for(int i=1;i<STANDART_AI_SIZE;i++)
                        {
                            Shablon[i] = ShablonStandartAI[i];
                            ui->textBrowser->append(Shablon[i]);
                            shablon_p = i;
                        }
                        isIMPchoosed = true; ui->checkBox_2->setChecked(isIMPchoosed);
                        gotHeader = true;
                    }
                    else
                    {
                        if (headerSize>=HEADER_SIZE_MAX)
                        {
                            ui->textBrowser->clear();
                            ui->textBrowser->append("Что-то не то в файле IMP. Слишком большой заголовок");
                            return;
                        }
                        Header[headerSize] = str1;
                        headerSize++;
                    }
                }

                posTASK = str1.indexOf("\\TASK/");//поиск названий тасков
                if (posTASK > 0)
                {
                    posTASK1 = str1.indexOf(("  ("));
                    if (posTASK1>posTASK)
                    {
                        str2 = str1.mid(posTASK + 6, posTASK1 - posTASK - 6);
                        posTASK = str2.indexOf("/");
                        if (posTASK==-1)
                        {
                            ui->comboBox->addItem(str2);
                            zPnt+=2;
                            Z[zPnt][0] = str2;
                        }
                    }
                }
                else
                {
                    if (!isGlobalFound)
                    {
                        int posVAR = str1.indexOf("VAR");
                        if (posVAR>0)
                        {
                            zPnt+=2;
                            Z[zPnt][0] = "GLOBAL";
                            isGlobalFound = true;
                        }
                    }
                }


                if (pos > 0)
                {
                    zPnt++;

                    zPos = str1.indexOf("VAR");
                    if (zPos > 0)
                    {
                        int zPosE = str1.lastIndexOf(" (");
                        int zPosB = zPos + 4;
                        Z[zPnt][0] = str1.mid(zPosB, zPosE - zPosB - 1);
                    }
                }
                else if (zPnt >= 0)
                {
                    for(int iSh = 1; iSh < STANDART_AI_SIZE_TOL-2; iSh++)
                    {
                        int zPosSh = ShablonStandartAITOL[iSh].indexOf(" = ");
                        QString strSh = ShablonStandartAITOL[iSh].mid(3, zPosSh);
                        zPos = str1.indexOf(strSh);
                        if (zPos >= 0)
                        {
                            int zPosE = str1.lastIndexOf("\"");
                            int zPosB = zPos + strSh.length();
                            QString strV = str1.mid(zPosB + 1, zPosE - zPosB - 1);
                            Z[zPnt][iSh] = strV;
                            break;
                        }
                    }
                }
            }
            cntZ = zPnt;
            fi.close();



        //записываем в файл настроек последний открытый путь
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
        QFile foS(settingsFile);
        if (!foS.open(QIODevice::WriteOnly)) return;
        QTextStream streamOS(&foS);
        QString path = ImpFile.left(ImpFile.lastIndexOf("/"));
        streamOS << path;
        streamOS << "\n";
        streamOS << pathC;
        streamOS << "\n";
        foS.close();
}





void MainWindow::on_pushButton_3_clicked()
{
    if (ui->checkBox_3->isChecked())
        {
            QString SaveFile = "";
            SaveFile = QFileDialog::getSaveFileName(this, tr("Введите имя файла"), "", "*.imp");
            if (SaveFile.length()>3)
            {
                QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
                QFile fout(SaveFile);
                if (!fout.open(QIODevice::WriteOnly)) return;
                QTextStream stream(&fout);
                for(int i=0; i < SaveLines.count(); i++)
                {
                    stream << SaveLines.value(i);
                    stream << "\n";
                }
                fout.close();
            }
        }//if check
}



void MainWindow::on_radioButton_clicked()
{
    if (ui->radioButton->isChecked())
        zamenaLevel = 0;
}

void MainWindow::on_radioButton_2_clicked()
{
    if (ui->radioButton_2->isChecked())
        zamenaLevel = 1;
}

void MainWindow::on_radioButton_3_clicked()
{
    if (ui->radioButton_3->isChecked())
        zamenaLevel = 2;
}

void MainWindow::on_radioButton_4_clicked()
{
    if (ui->radioButton_4->isChecked())
        zamenaLevel = 4;
}

void MainWindow::on_radioButton_5_clicked()
{
    if (ui->radioButton_5->isChecked())
        zamenaLevel = 3;
}



void MainWindow::on_pushButton_2_clicked()
{
    if (ui->checkBox_2->isChecked())
        {
            QString SaveFile = "";
            SaveFile = QFileDialog::getSaveFileName(this, tr("Введите имя файла"), ImpFile.left(ImpFile.length()-3)+"csv", "*.csv");
            if (SaveFile.length()>3)
            {
                QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
                QFile fout(SaveFile);
                if (!fout.open(QIODevice::WriteOnly)) return;
                QTextStream stream(&fout);
                for(int i=0; i < cntZ + 1; i++)
                {
                    for(int p=0; p<STANDART_AI_SIZE_TOL-2; p++)
                    {
                        stream << Z[i][p];
                        stream << ";";
                    }
                    stream << "\n";
                }
                fout.close();
            }
        }//if check
}

void MainWindow::on_pushButton_6_clicked()//просто заменить русские буквы
{
    QString settingsFile = "settings.apr";
    bool isGlobalFound = false;
        //читаем из файла настроек последний открытый путь
        QFile fiS(settingsFile);
        QString pathI = "";
        QString pathC = "";
        if (fiS.open(QIODevice::ReadOnly))
        {
            QTextStream streamIS;
            streamIS.setCodec("UTF-8");
            streamIS.setDevice(&fiS);
            if (!streamIS.atEnd())
                      pathI = streamIS.readLine();//первая запись - для imp
            if (!streamIS.atEnd())
                      pathC = streamIS.readLine();
            fiS.close();
        }



        ImpFile = "";
            ImpFile = QFileDialog::getOpenFileName(this, tr("Выберите экспортированный IMP-файл"), pathI, "*.imp");
            if (ImpFile.length()<3) return;

            for(int iz=0;iz<MAX_STR_CNT;iz++)
                for(int pz=0;pz<11;pz++)
                    Z[iz][pz] = "";

            ui->textBrowser->clear();
            ui->comboBox->clear();
            ui->comboBox->addItem("GLOBAL");


            //смотрим введены ли какие-нибудь конкретные номера сигналов
            QString strSignalNumbers = ui->lineEdit_2->text();

            QList<QString>numlist = strSignalNumbers.split(QRegExp("[;, ]"));


            int usercount = numlist.count();
            bool isFound = false;



            QFile fi(ImpFile);
            if (!fi.open(QIODevice::ReadOnly)) return;
            QTextStream stream;
            QString str1,str2,str3;
            stream.setCodec("UTF-8");
            stream.setDevice(&fi);

            QString OutFile = ImpFile.left(ImpFile.length()-4) + "_Out.imp";
            QFile fo(OutFile);
            if (!fo.open(QIODevice::WriteOnly)) return;
            QTextStream outstream;
            outstream.setCodec("UTF-8");
            outstream.setDevice(&fo);

            int pos = 0;
            int posTASK, posTASK1;
            bool gotHeader = false;
            headerSize = 0;
            int zPnt = -1;
            int zPos = -1;
            QStringList strL;

            while (!stream.atEnd())
            {
                str1 = stream.readLine();

                if (isFound)
                {
                    pos = str1.indexOf("VAR");
                    if (pos > 0)
                    {
                        isFound = false;
                    }
                }

                if ((usercount>0)&&(!isFound))
                {
                    isFound = false;
                    while(!isFound)
                    {
                        pos = str1.indexOf("VAR");
                        if (pos > 0)
                        {
                            str2 = str1.right(str1.length() - pos);
                            strL = str2.split(QRegExp("[^0-9]"));
                            for(int i=strL.count()-1 ; i>=0 ; i--)
                            {
                                if (strL[i].length()>0)
                                {
                                    str3 = strL[i];
                                    break;
                                }
                            }
                            //str3 - нашли номер переменной в текущей строке imp

                            for(int i=0; i< usercount; i++)
                            {
                                if (str3 == numlist[i])
                                {
                                    isFound = true;
                                    break;
                                }
                            }

                            if (!isFound)
                            {
                                pos = -1;
                                while (pos<=0)
                                {
                                    str1 = stream.readLine();
                                    pos = str1.indexOf("VAR");
                                    if (stream.atEnd()) break;
                                }
                            }

                        }
                        else
                        {
                            break;
                        }
                    }
                }


                pos = str1.indexOf("desc0");
                if (pos>0)
                {
                    posTASK = str1.indexOf("\"");
                    posTASK1 = str1.lastIndexOf("\"");
                    switch (zamenaLevel) {
                    case 0:
                        str3 = str1.mid(posTASK, posTASK1 - posTASK);
                    break;
                    case 1:
                        str3 = ZamenaLight(str1.mid(posTASK, posTASK1 - posTASK));
                    break;
                    case 2:
                        str3 = Zamena(str1.mid(posTASK, posTASK1 - posTASK));
                    break;
                    case 3:
                        str3 = ZamenaPlus(str1.mid(posTASK, posTASK1 - posTASK));
                    break;
                    case 4:
                        str3 = HardZamena(str1.mid(posTASK, posTASK1 - posTASK));
                    break;
                    }
                    str2 = str1.left(posTASK) + str3 + str1.right(str1.length() - posTASK1);

                    ui->textBrowser->append(str2);
                    outstream << str2;
                    outstream << endl;
                }
                else
                {
                    ui->textBrowser->append(str1);
                    outstream << str1;
                    outstream << endl;
                }
            }
            cntZ = zPnt;
            fi.close();

        ui->textBrowser->append("Сохранено в "  + OutFile);

        //записываем в файл настроек последний открытый путь
        QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
        QFile foS(settingsFile);
        if (!foS.open(QIODevice::WriteOnly)) return;
        QTextStream streamOS(&foS);
        QString path = ImpFile.left(ImpFile.lastIndexOf("/"));
        streamOS << path;
        streamOS << "\n";
        streamOS << pathC;
        streamOS << "\n";
        foS.close();

}
