/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QCheckBox *checkBox;
    QCheckBox *checkBox_3;
    QCheckBox *checkBox_4;
    QTextBrowser *textBrowser;
    QLineEdit *lineEdit;
    QCheckBox *checkBox_2;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QLabel *label;
    QLineEdit *lineEdit_2;
    QRadioButton *radioButton_5;
    QComboBox *comboBox;
    QPushButton *pushButton_2;
    QPushButton *pushButton_6;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(20, 10, 171, 26));
        pushButton_3 = new QPushButton(centralwidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(20, 150, 171, 26));
        pushButton_4 = new QPushButton(centralwidget);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(20, 50, 171, 26));
        pushButton_5 = new QPushButton(centralwidget);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(50, 100, 141, 31));
        checkBox = new QCheckBox(centralwidget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setGeometry(QRect(200, 10, 21, 24));
        checkBox_3 = new QCheckBox(centralwidget);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setGeometry(QRect(200, 150, 21, 24));
        checkBox_4 = new QCheckBox(centralwidget);
        checkBox_4->setObjectName(QString::fromUtf8("checkBox_4"));
        checkBox_4->setGeometry(QRect(370, 10, 261, 24));
        checkBox_4->setChecked(true);
        textBrowser = new QTextBrowser(centralwidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(10, 210, 771, 341));
        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(650, 10, 113, 26));
        checkBox_2 = new QCheckBox(centralwidget);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setGeometry(QRect(200, 50, 21, 24));
        radioButton = new QRadioButton(centralwidget);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));
        radioButton->setGeometry(QRect(370, 60, 102, 24));
        radioButton->setChecked(true);
        radioButton_2 = new QRadioButton(centralwidget);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
        radioButton_2->setGeometry(QRect(370, 80, 102, 24));
        radioButton_3 = new QRadioButton(centralwidget);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));
        radioButton_3->setGeometry(QRect(370, 100, 102, 24));
        radioButton_4 = new QRadioButton(centralwidget);
        radioButton_4->setObjectName(QString::fromUtf8("radioButton_4"));
        radioButton_4->setGeometry(QRect(370, 140, 102, 24));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(320, 40, 141, 18));
        lineEdit_2 = new QLineEdit(centralwidget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(360, 170, 301, 26));
        radioButton_5 = new QRadioButton(centralwidget);
        radioButton_5->setObjectName(QString::fromUtf8("radioButton_5"));
        radioButton_5->setGeometry(QRect(370, 120, 102, 24));
        comboBox = new QComboBox(centralwidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(610, 50, 151, 26));
        pushButton_2 = new QPushButton(centralwidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setEnabled(true);
        pushButton_2->setGeometry(QRect(220, 80, 131, 26));
        pushButton_6 = new QPushButton(centralwidget);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(669, 170, 111, 26));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 23));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "open CSV", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214 \320\263\320\276\321\202\320\276\320\262\321\213\320\271 IMP", nullptr));
        pushButton_4->setText(QApplication::translate("MainWindow", "\320\276\321\202\320\272\321\200\321\213\321\202\321\214 \321\215\320\272\321\201\320\277\320\276\321\200\321\202 IMP", nullptr));
        pushButton_5->setText(QApplication::translate("MainWindow", "\320\266\320\274\320\270 \320\275\320\260 \320\263\320\260\320\267", nullptr));
        checkBox->setText(QString());
        checkBox_3->setText(QString());
        checkBox_4->setText(QApplication::translate("MainWindow", "\320\277\321\200\320\276\320\277\321\203\321\201\321\202\320\270\321\202\321\214 \320\277\320\265\321\200\320\262\321\203\321\216 \321\201\321\202\321\200\320\276\320\272\321\203 CSV \321\204\320\260\320\271\320\273\320\260", nullptr));
        lineEdit->setText(QApplication::translate("MainWindow", "APROLKEY", nullptr));
        checkBox_2->setText(QString());
        radioButton->setText(QApplication::translate("MainWindow", "\320\275\320\265 \320\274\320\265\320\275\321\217\321\202\321\214", nullptr));
        radioButton_2->setText(QApplication::translate("MainWindow", "\320\273\320\260\320\271\321\202", nullptr));
        radioButton_3->setText(QApplication::translate("MainWindow", "\321\201\321\200\320\265\320\264\320\275\321\217\321\217", nullptr));
        radioButton_4->setText(QApplication::translate("MainWindow", "\320\267\320\260\320\261\320\276\321\200\320\270\321\201\321\202\320\260\321\217", nullptr));
        label->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\274\320\265\320\275\320\260 \321\200\321\203\321\201\321\201\320\272\320\270\321\205 \320\261\321\203\320\272\320\262:", nullptr));
        lineEdit_2->setText(QApplication::translate("MainWindow", "\320\222\321\201\320\265", nullptr));
        radioButton_5->setText(QApplication::translate("MainWindow", "\321\201\321\200\320\265\320\264\320\275\321\217\321\217 + \320\271", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "\320\222\320\276\321\201\321\201\321\202\320\260\320\275\320\276\320\262\320\270\321\202\321\214 CSV", nullptr));
        pushButton_6->setText(QApplication::translate("MainWindow", "\320\267\320\260\320\274\320\265\320\275\320\270\321\202\321\214 \320\262 IMP", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
